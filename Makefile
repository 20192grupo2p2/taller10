bin/matmul: src/matriz.o
	gcc -Wall -fsanitize=address,undefined -pthread $+ -o $@

.PHONY: clean
clean:
	rm -rf bin obj 
	mkdir bin obj

exe:
		
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 1
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 1
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 1	
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 2
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 2
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 2	
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 3
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 3
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 3
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 4
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 4
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 4
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 5	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 5
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 5
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 6
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 6
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 6
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 7
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 7
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 7
	
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 8
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 8
	bin/matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 8
